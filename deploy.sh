avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega2560 -c -o led.o led.c
avr-gcc -mmcu=atmega2560 led.o -o led.elf
avr-objcopy -O ihex -R .eeprom led.elf led.hex
avrdude -F -V -cwiring -patmega2560 -P/dev/ttyACM0 -b115200 -Uflash:w:led.hex
