/*
* This code can be run on an Arduino Mega2560 or clone chip.
* It blinks 3 leds
*/
#include <avr/io.h>
#include <util/delay.h>

#define BLINK_DELAY_MS 100
#define FINAL_DELAY_MS 500


int main(void) {
	/*
	* pin PL0 - green led
	* pin PL2 - red led
	* pin PL6 - blue led
	*/
	/* set pins 0,2,6 of PORTL for output*/
	DDRL |= _BV(DDL2);
	DDRL |= _BV(DDL0);
	DDRL |= _BV(DDL6);

	while (1) {
		/* Turn on green led */
		PORTL |= _BV(PORTL0);
		_delay_ms(BLINK_DELAY_MS);

		/* Turn off green led */
		PORTL &= ~_BV(PORTL0);
		/* Turn on red led */
		PORTL |= _BV(PORTL2);
		_delay_ms(BLINK_DELAY_MS);

		/* Turn off red led */
		PORTL &= ~_BV(PORTL2);
		/* Turn on blue led */
		PORTL |= _BV(PORTL6);
		_delay_ms(BLINK_DELAY_MS);

		/* Turn off blue led and a little more*/
		PORTL &= ~_BV(PORTL6);
		_delay_ms(FINAL_DELAY_MS);
	}
}
